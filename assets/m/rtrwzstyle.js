
var defaultImageStyle = new ol.style.Circle({
    radius: 5,
    fill: null,
    stroke: new ol.style.Stroke({ color: 'red', width: 1 })
});

function DkpZone(feature) {
    this.feature = feature
}
DkpZone.prototype.getTheStyle = function () {
    let style
    switch (this.feature.getGeometry().getType()) {
        case 'Point':
            style = new ol.style.Style({
                image: defaultImageStyle
            })
            break
        case 'LineString':
            style = new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: this.lineColorForLineType(),
                    width: 2,
                    lineDash: this.lineDashStyle() 
                })
            })
            break
        case 'MultiLineString':
            style = new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: this.lineColorForLineType(),
                    width: 2,
                    lineDash: this.lineDashStyle() 
                })
            })
            break
        case 'MultiPoint':
            style = new ol.style.Style({
                image: defaultImageStyle
            })
            break
        case 'MultiPolygon':
            style = new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: this.lineColorPoly(),
                    width: 2,
                    lineDash: this.lineDashStyle() 
                }),
                fill: new ol.style.Fill({
                    color: this.fillColor()
                })
            })
            break
        case 'Polygon':
            style = new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: this.lineColorPoly(),
                    lineDash: [4],
                    width: 3,
                    lineDash: this.lineDashStyle() 
                }),
                fill: new ol.style.Fill({
                    color: this.fillColor()
                })
            })
            break
        case 'GeometryCollection':
            style = new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: this.lineColorPoly(),
                    width: 2,
                    lineDash: this.lineDashStyle() 
                }),
                fill: new ol.style.Fill({
                    color: this.fillColor()
                }),
                image: new ol.style.Circle({
                    radius: 10,
                    fill: null,
                    stroke: new ol.style.Stroke({
                        color: 'magenta'
                    })
                })
            })
            break
        case 'Circle':
            style = new ol.style.Style({
                stroke: new ol.style.Stroke({
                    color: this.lineColorPoly(),
                    width: 2,
                    lineDash: this.lineDashStyle() 
                }),
                fill: new ol.style.Fill({
                    color: this.fillColor()
                })
            })
            break
    }
    return style
}
// untuk warna garis
DkpZone.prototype.lineColorForLineType = function() {
    let KODE_SZ = this.feature.get('KODE_SZ')
    if (KODE_SZ.indexOf('AL-APK-KT') == 0) {
        return "rgba(255, 0, 102, 1)";
    } else if (KODE_SZ.indexOf('AL-AP-PR') == 0) {
        return "rgba(152, 230, 0, 1)";
    } else if (KODE_SZ.indexOf('AL-APK-GM') == 0) {
        return "rgba(255, 0, 102, 1)";
    } else if (KODE_SZ.indexOf('AL-AP-PN') == 0) {
        return "rgba(204, 153, 255, 1)";
    } else if (KODE_SZ.indexOf('AL-AMB-MM') == 0) {
        return "rgba(46, 117, 182, 1)";
    } else if (KODE_SZ.indexOf('AL-AMB-MP') == 0) {
        return "rgba(51, 153, 51, 1)";
    }
    return 'black'
}
DkpZone.prototype.fillColor = function() {
    let KODE_SZ = this.feature.get('KODE_SZ')

    if (KODE_SZ.indexOf('KPU-TB') == 0) {
        return "rgba(242, 242, 242, 1)";
    } else if (KODE_SZ.indexOf('KPU-PT-PD') == 0) {
        return "rgba(255, 211, 127, 1)";
    } else if (KODE_SZ.indexOf('KPU-PL-DLK') == 0) {
        return "rgba(146, 205, 220, 1)";
    } else if (KODE_SZ.indexOf('KPU-W-P3K') == 0) {
        return "rgba(255, 220, 255, 1)";
    } else if (KODE_SZ.indexOf('AL-APK-GM') == 0) {
        return "rgba(255, 0, 102, 1)";
    } else if (KODE_SZ.indexOf('KKP') == 0) {
        return "rgba(85, 255, 0, 1)";
    } else if (KODE_SZ.indexOf('KPU-W-BD') == 0) {
        return "rgba(255, 220, 255, 1)";
    } else if (KODE_SZ.indexOf('KPU-PB-BL') == 0) {
        return "rgba(0, 197, 255, 1)";
    }
    return 'orange'
}
// untuk dashline
DkpZone.prototype.lineDashStyle = function () {
    let kodeSZ = this.feature.get('KODE_SZ')
    if (kodeSZ.indexOf('AL-APK-GM') == 0) {
        return [3, 10];
    } else if (kodeSZ.indexOf('AL-AMB-MM') == 0) {
        return [3, 10];
    } else if (kodeSZ.indexOf('AL-AMB-MP') == 0) {
        return [3, 10];
    } else if (kodeSZ.indexOf('AL-APK-GM') == 0) {
        return [3, 10];
    }
    // default tidak ada!
    return undefined
}
// untuk warna garis di poly
DkpZone.prototype.lineColorPoly = function() {
    let kodeSZ = this.feature.get('KODE_SZ')
    if (kodeSZ.indexOf('AL-APK-GM') == 0) {
        return [197,0,255, 0.6];
    } else if (kodeSZ.indexOf('KPU-W-BD') == 0) {
        return [255, 255, 0, 0.6];
    }
    return 'black'
}

var styleGeojsonKita = function (feature, resolution) {
    return (new DkpZone(feature)).getTheStyle()
}
// ini saat mouse over apa stylenya?
var highlightStyle = new ol.style.Style({
    stroke: new ol.style.Stroke({
        color: [0, 0, 0, 0.6],
        width: 2
    }),
    fill: new ol.style.Fill({
        color: [255, 0, 0, 0.2]
    }),
    zIndex: 1
})

// ini saat terpilih
var selectedStyle = new ol.style.Style({
    stroke: new ol.style.Stroke({
        color: 'red',
        width: 2
    }),
    fill: new ol.style.Fill({
        color: 'red'
    }),
    zIndex: 2
})

var styleGeojsonHighlightFunction = function (feature, resolution) {
    // next aja 
}