var UtilFormatTampilan = {
    formatProperties: function(props) {
        let s = ''
        for(let prop in props) {
            if( props[prop] instanceof Object) {
                continue
            }
            s += "<strong>" + prop + "</strong>:" + props[prop] + "<br>"
        }
        return s
    }, 
    keteranganLengkap: function(props) {
        return 'Zona: ' + props.ZONA + ' SubZona:' + props.SUBZONA + ' ' + props.KETERANGAN 
            + ' TAHUN ' + props.TAHUN

    }
}